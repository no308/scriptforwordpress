#!/bin/bash -e

cat /etc/os-release

echo "Votre hote mariadb?"
read DbHost
echo $DbHost

sudo apt -y update

# php Installation  
sudo apt -y install php php-cgi php-mysqli php-pear php-mbstring libapache2-mod-php php-common php-phpseclib php-mysql

# Apache Installation  
sudo apt -y install apache2

# creation phpmyadmin folder
sudo mkdir /var/www/html/phpmyadmin /var/www/html/phpmyadmin/tmp
# installing  phpmyadmin
sudo wget https://files.phpmyadmin.net/phpMyAdmin/5.2.0/phpMyAdmin-5.2.0-all-languages.tar.gz
# decompress phpmyadmin
sudo tar xvf phpMyAdmin-5.2.0-all-languages.tar.gz
# move to www
sudo mv phpMyAdmin-*/* /var/www/html/phpmyadmin
# cleaning downloaded folders
sudo rm -rf phpMy*

#config phpmyadmin
cd /var/www/html/
# we will rather generate a file from 0 that we can adapt to the mariadb database name variable
sudo touch phpmyadmin/config.inc.php 

sudo chmod a+w phpmyadmin/config.inc.php
sudo chown -R www-data:www-data phpmyadmin/

############################################
# supply of the config file with cookie and adapted variable !!
#########################################

leCookie=$(openssl rand -base64 32 | head -c 32)
sudo echo "<?php" >> phpmyadmin/config.inc.php
sudo echo "declare(strict_types=1);" >> phpmyadmin/config.inc.php
sudo echo "\$cfg['blowfish_secret']='$leCookie';" >> phpmyadmin/config.inc.php
sudo echo "\$i = 0;" >> phpmyadmin/config.inc.php
sudo echo "\$i++;" >> phpmyadmin/config.inc.php
sudo echo "\$cfg['Servers'][\$i]['auth_type']='cookie';" >> phpmyadmin/config.inc.php

sudo echo "\$cfg['Servers'][\$i]['host']='$DbHost';" >> phpmyadmin/config.inc.php
sudo echo "\$cfg['Servers'][\$i]['compress']='false';" >> phpmyadmin/config.inc.php
sudo echo "\$cfg['Servers'][\$i]['AllowNoPassword']='false';" >> phpmyadmin/config.inc.php
sudo echo "\$cfg['UploadDir']='';" >> phpmyadmin/config.inc.php
sudo echo "\$cfg['SaveDir']='';" >> phpmyadmin/config.inc.php
sudo echo "\$cfg['TempDir']='/var/www/html/phpmyadmin/tmp';" >> phpmyadmin/config.inc.php

#remove write permissions in config file
sudo chmod a-w phpmyadmin/config.inc.php

#restart apache
sudo service apache2 restart
sudo apt install mariadb-server