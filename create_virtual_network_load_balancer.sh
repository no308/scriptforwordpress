#Login to azure

az login

#Create resource group

echo "Enter a resource group name"
read nameRG
echo $nameRG
#--------------------------------------------------------------------

echo "Enter a name mariadb server"
read nameDB
echo $nameDB
#--------------------------------------------------------------------

echo "Enter mariadb admin username"
read nameUserDB
echo $nameUserDB
#--------------------------------------------------------------------

echo "Enter mariadb admin password"
read nameUserPassword
echo $nameUserPassword
#--------------------------------------------------------------------

az group create \
    --name $nameRG \
    --location westus3

    #Create virtual network

    az network vnet create \
    --resource-group $nameRG \
    --location westus3 \
    --name myVNet \
    --address-prefixes 10.1.0.0/16 \
    --subnet-name myBackendSubnet \
    --subnet-prefixes 10.1.0.0/24

    #Create a public IP address

   az network public-ip create \
    --resource-group $nameRG \
    --name myPublicIP \
    --sku Standard \
    --zone 1

    #Create the load balancer resource

     az network lb create \
    --resource-group $nameRG \
    --name myLoadBalancer \
    --sku Standard \
    --public-ip-address myPublicIP \
    --frontend-ip-name myFrontEnd \
    --backend-pool-name myBackEndPool

   #Create the load balancer resource


    #Create the health probe

    az network lb probe create \
    --resource-group $nameRG \
    --lb-name myLoadBalancer \
    --name myHealthProbe \
    --protocol tcp \
    --port 80

    #Create a load balancer rule

     az network lb rule create \
    --resource-group $nameRG \
    --lb-name myLoadBalancer \
    --name myHTTPRule \
    --protocol tcp \
    --frontend-port 80 \
    --backend-port 80 \
    --frontend-ip-name myFrontEnd \
    --backend-pool-name myBackEndPool \
    --probe-name myHealthProbe \
    --disable-outbound-snat true \
    --idle-timeout 15 \
    --enable-tcp-reset true


    #Create a network security group

    az network nsg create \
    --resource-group $nameRG \
    --name myNSG

    #Create a network security group rule

     az network nsg rule create \
    --resource-group $nameRG \
    --nsg-name myNSG \
    --name myNSGRuleHTTP \
    --protocol '*' \
    --direction inbound \
    --source-address-prefix '*' \
    --source-port-range '*' \
    --destination-address-prefix '*' \
    --destination-port-range 80 \
    --access allow \
    --priority 200

#Create a bastion host:
    
     #Create NAT gateway

    az network public-ip create \
        --resource-group $nameRG \
        --name myBastionIP \
        --sku Standard \
        --zone 1


   # Create a bastion subnet

      az network vnet subnet create \
    --resource-group $nameRG \
    --name AzureBastionSubnet \
    --vnet-name myVNet \
    --address-prefixes 10.1.1.0/27
    

    #Create bastion host

     az network bastion create \
    --resource-group $nameRG \
    --name myBastionHost \
    --public-ip-address myBastionIP \
    --vnet-name myVNet \
    --location westus3

    #Create backend servers
        #Create network interfaces for the virtual machines

          array=(myNicVM1 myNicVM2)
  for vmnic in "${array[@]}"
  do
    az network nic create \
        --resource-group $nameRG \
        --name $vmnic \
        --vnet-name myVNet \
        --subnet myBackEndSubnet \
        --network-security-group myNSG
  done
    
    #Create an Azure Database for MariaDB server

    az mariadb server create --resource-group $nameRG --name $nameDB  --location westus3 --admin-user $nameUserDB --admin-password $nameUserPassword --sku-name GP_Gen5_2 --version 10.2

    #Configure a firewall rule

    az mariadb server firewall-rule create --resource-group $nameRG --server $nameDB --name AllowMyIP --start-ip-address 192.168.0.1 --end-ip-address 192.168.0.1

    #Configure SSL settings

    az mariadb server update --resource-group $nameRG --name $nameDB --ssl-enforcement Disabled

    az mariadb server show --resource-group $nameRG --name $nameDB

    #Connect to the server by using the mysql command-line tool

    #mysql -h mydemoserver.mariadb.database.azure.com -u myadmin@mydemoserver -p

    #Create virtual machine

        az vm create \
        --resource-group $nameRG  \
        --name myVM \
        --image Debian:debian-11:11-gen2:0.20220503.998 \
        --admin-username $nameVm  \
 	--count 2 \
        --generate-ssh-keys


#Open port 80 for web traffic

az vm open-port --port 80 --resource-group $nameRG --name myVM0
az vm open-port --port 80 --resource-group $nameRG --name myVM1


     #add_virtual machines to load balancer backend pool

   array=(myNicVM1 myNicVM2)
  for vmnic in "${array[@]}"
  do
    az network nic ip-config address-pool add \
     --address-pool myBackendPool \
     --ip-config-name ipconfig1 \
     --nic-name $vmnic \
     --resource-group $nameRG \
     --lb-name myLoadBalancer
  done

#Create NAT gateway:

    #Create public IP

   az network public-ip create \
    --resource-group $nameRG \
    --name myNATgatewayIP \
    --sku Standard \
    --zone 1

#Create NAT gateway resource
    
     az network nat gateway create \
    --resource-group $nameRG \
    --name myNATgateway \
    --public-ip-addresses myNATgatewayIP \
    --idle-timeout 10

#Associate NAT gateway with subnet

    az network vnet subnet update \
    --resource-group $nameRG \
    --vnet-name myVNet \
    --name myBackendSubnet \
    --nat-gateway myNATgateway

#_add a multiple VMs inbound NAT rule

  az network lb inbound-nat-rule create \
        --backend-port 22 \
        --lb-name myLoadBalancer \
        --name myInboundNATrule \
        --protocol Tcp \
        --resource-group $nameRG \
        --backend-pool-name myBackendPool \
        --frontend-ip-name myFrontend \
        --frontend-port-range-end 1000 \
        --frontend-port-range-start 500
