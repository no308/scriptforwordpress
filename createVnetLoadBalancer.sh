#Login to azure

az login

#Create resource group

echo "Enter a resource group name"
read nameRG

#---------------------------------------------------------------------

echo "Enter a name for virtual network"
read nameVnet

#----------------------------------------------------------------------

echo "Enter a name for backend subnet"
read nameBackSnet

#---------------------------------------------------------------------

echo "Enter a name for BastionIP"
read nameBastionIP

#---------------------------------------------------------------------

echo "Enter a name for Bastion subnet"
read nameBastionSubnet

#---------------------------------------------------------------------

echo "Enter a name for load balancer"
read nameLB

#---------------------------------------------------------------------

echo "Enter a name for Front End ip"
read nameFrontEndIP

#---------------------------------------------------------------------

echo "Enter a name for BackEndPool"
read nameBackEndPool

#----------------------------------------------------------------------

echo "Enter a name for Health Probe"
read nameHP

#---------------------------------------------------------------------

echo "Enter a name for network security group"
read nameNSG

#---------------------------------------------------------------------

echo "Enter a name for HTTP Rule"
read nameHTTPRule

#---------------------------------------------------------------------

echo "Enter a name for Network security Rule HTTP"
read nameNSGRuleHTTP

#---------------------------------------------------------------------

echo "Enter a name NAT gateway IP"
read nameNATGateWayIP

#---------------------------------------------------------------------

echo "Enter a name NAT gateway"
read nameNATGateway

#---------------------------------------------------------------------

echo "Enter a name mariadb server"
read nameDB

#--------------------------------------------------------------------

echo "Enter mariadb admin username"
read nameUserDB

#--------------------------------------------------------------------

echo "Enter mariadb admin password"
read nameUserPassword

#--------------------------------------------------------------------

echo "Enter a name to create multiple virtual machine"
read nameVm

#--------------------------------------------------------------------

echo "Enter number of virtual machine instances"
read vmNum

az group create \
    --name $nameRG \
    --location westus3

    #Create virtual network

    az network vnet create \
    --resource-group $nameRG \
    --location westus3 \
    --name $nameVnet \
    --address-prefixes 10.1.0.0/16 \
    --subnet-name $nameBackSnet \
    --subnet-prefixes 10.1.0.0/24

    #Create a bastion public IP address

    az network public-ip create \
    --resource-group $nameRG  \
    --name $nameBastionIP \
    --sku Standard \
    --zone 1 2 3

    #Create a bastion subnet

    az network vnet subnet create \
    --resource-group $nameRG  \
    --name $nameBastionSubnet \
    --vnet-name $nameVnet \
    --address-prefixes 10.1.1.0/27

   #Create the load balancer resource

   az network lb create \
    --resource-group $nameRG \
    --name $nameLB \
    --sku Standard \
    --vnet-name $nameVnet \
    --subnet $nameBackSnet \
    --frontend-ip-name $nameFrontEndIP \
    --backend-pool-name $nameBackEndPool

    #Create the health probe

    az network lb probe create \
    --resource-group $nameRG \
    --lb-name $nameLB \
    --name $nameHP \
    --protocol tcp \
    --port 80

    #Create a load balancer rule

    az network lb rule create \
    --resource-group $nameRG \
    --lb-name $nameLB \
    --name $nameHTTPRule \
    --protocol tcp \
    --frontend-port 80 \
    --backend-port 80 \
    --frontend-ip-name $nameFrontEndIP \
    --backend-pool-name $nameBackEndPool \
    --probe-name $nameHP \
    --idle-timeout 15 \
    --enable-tcp-reset true


    #Create a network security group

    az network nsg create \
    --resource-group $nameRG \
    --name $nameNSG

    #Create a network security group rule

    az network nsg rule create \
    --resource-group $nameRG \
    --nsg-name $nameNSG \
    --name $nameNSGRuleHTTP \
    --protocol '*' \
    --direction inbound \
    --source-address-prefix '*' \
    --source-port-range '*' \
    --destination-address-prefix '*' \
    --destination-port-range 80 \
    --access allow \
    --priority 200


    #Create NAT gateway

az network public-ip create \
    --resource-group $nameRG \
    --name $nameNATGateWayIP \
    --sku Standard \
    --zone 1 2 3


    #Create NAT gateway resource
    

    az network nat gateway create \
    --resource-group $nameRG \
    --name $nameNATGateway \
    --public-ip-addresses $nameNATGateWayIP \
    --idle-timeout 15

    #Associate NAT gateway with subnet

    az network vnet subnet update \
    --resource-group $nameRG \
    --vnet-name $nameVnet \
    --name $nameBackSnet \
    --nat-gateway $nameNATGateway

    
    #Create an Azure Database for MariaDB server

    az mariadb server create --resource-group $nameRG --name $nameDB  --location westus --admin-user $nameUserDB --admin-password $nameUserPassword --sku-name GP_Gen5_2 --version 10.2

    #Configure a firewall rule

    az mariadb server firewall-rule create --resource-group $nameRG --server $nameDB --name AllowMyIP --start-ip-address 192.168.0.1 --end-ip-address 192.168.0.1

    #Configure SSL settings

    az mariadb server update --resource-group $nameRG --name $nameDB --ssl-enforcement Disabled

    az mariadb server show --resource-group $nameRG --name $nameDB

    #Connect to the server by using the mysql command-line tool

    #mysql -h mydemoserver.mariadb.database.azure.com -u myadmin@mydemoserver -p

    #Create virtual machine

    #Create virtual machine

    for((i=0 ; i<=$vmNum ; i++)); do
        az vm create \
        --resource-group $nameRG  \
        --name $nameVm$i \
        --image Debian \
        --admin-username $nameVm  \
        --generate-ssh-keys
    done

#Open port 80 for web traffic

az vm open-port --port 80 --resource-group $nameRG --name $nameVm

     #add_virtual machines to load balancer backend pool

  for ((i=0 ; i<=$vmNum ; i++)); do
      az network nic ip-config address-pool add \
     --address-pool $nameBackEndPool \
     --ip-config-name ipconfig1 \
     --nic-name $nameVm$1 \
     --resource-group $nameRG \
     --lb-name $nameLB
  done